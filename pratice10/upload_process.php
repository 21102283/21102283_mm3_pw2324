<?php
    $target_dir = "uploads/";
    $target_file = $target_dir . basename($_FILES["gambar_contoh"]["name"]);
    $error = false;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    // memvalidasi apakah variabel $_POST[‘submit’] benar-benar ada, jika ada maka akan dilakukan validasi kembali apakah file yang akan diupload sudah tersimpan di penyimpanan temporary, jika ya maka kondisi error tetap false.
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["gambar_contoh"]["tmp_name"]);
        if($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $error = false;
        } else {
            echo "File is not an image.";                $error = false;
        }
    }
    
    // untuk memvalidasi apakah sudah terdapat file dengan nama yang sama atau tidak di folder uploads, jika ya maka kondisi error menjadi true, jika tidak kondisi error tetap false.
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $error = true;
    }

    //untuk memvalidasi apakah ukuran file lebih dari 500kb, jika ya maka kondisi error menjadi true, jika tidak kondisi error tetap false.
    if ($_FILES["gambar_contoh"]["size"] > 500000) {
         echo "Sorry, your file is too large.";
        $error = true;
    }
    
    //untuk memvalidasi ekstensi file yang akan diupload bukan jpg/png/jpeg/gif, jika ya maka kondisi error menjadi true, jika tidak kondisi error tetap false.
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $error = true;
    }

    //  validasi apakah error berisi true, jika ya maka proses upload tidak akan dilakukan, jika error berisi false maka lakukan proses upload
    if ($error == true) {
        echo "Sorry, your file was not uploaded.";
    } else {
        if (move_uploaded_file($_FILES["gambar_contoh"]["tmp_name"], $target_file)) {
            echo "The file ". basename( $_FILES["gambar_contoh"]["name"]). " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
?>







