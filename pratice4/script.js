// Ambil elemen dengan ID "myButton"
var button = document.getElementById("myButton");

// Tambahkan event listener untuk mengubah teks saat tombol diklik
button.addEventListener("click", function() {
    document.getElementById("demo").innerHTML = "Tombol telah diklik!";
});